package ukulelechordconverter;

import static org.junit.Assert.*;

import org.junit.Test;

public class UkuleleChordConverterTest {

	@Test
	public void test1() {
		String org = "C";
		String trans = "F";
		String chord = "Am7";
		try{
			UkuleleChordConverter conv = new UkuleleChordConverter(org, trans);
			System.out.println(conv.convert(chord));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Test(expected = NotSuffixException.class)
	public void test2() {
		String org = "c";
		String trans = "F";
		String chord = "Am7";
		try{
			UkuleleChordConverter conv = new UkuleleChordConverter(org, trans);
			System.out.println(conv.convert(chord));
		}catch(Exception e){
			e.toString();
		}
	}
	
	
}
