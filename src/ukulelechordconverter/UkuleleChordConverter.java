package ukulelechordconverter;
import java.util.Arrays;
import java.util.HashMap;

public class UkuleleChordConverter {
	
	private HashMap<String, Integer> chord = new HashMap<String, Integer>();
	final String[] chordS = {"C","C#","D","D#","E", "F","F#","G","G#","A","A#","B", "C"};
	final String[] chordF = {"C","Db","D","Eb","E", "F","Gb","G","Ab","A","Bb","B", "C"};
	final String[] suffix = {"m7-5", "maj7", "sus4", "dim", "aug", "m7","m6", "6","7", "m", "9"};
	private String original, trans;
	private int diff;
	
	public UkuleleChordConverter(String original, String trans) throws NotChordException{
	  this.chord.put("C", 0);
	  this.chord.put("C#", 1);
	  this.chord.put("Db", 1);
	  this.chord.put("D", 2);
	  this.chord.put("D#", 3);
	  this.chord.put("Eb", 3);
	  this.chord.put("E", 4);
	  this.chord.put("F", 5);
	  this.chord.put("F#", 6);
	  this.chord.put("Gb", 6);
	  this.chord.put("G", 7);
	  this.chord.put("G#", 8);
	  this.chord.put("Ab", 8);
	  this.chord.put("A", 9);
	  this.chord.put("A#", 10);
	  this.chord.put("Bb", 10);
	  this.chord.put("B", 11);
	  this.chord.put("C", 12);
	  
	  int original_key, trans_key = 0;
	  try{
		  original_key = this.chord.get(original);
	  }catch(Exception e){
		  throw new NotChordException(original);
	  }
	
	  try{
		  trans_key = this.chord.get(trans);
	  }catch(Exception e){
		  throw new NotChordException(trans);
	  }
	  
	  this.original = original;
	  this.trans = trans;
	  
	  this.diff = trans_key - original_key;
	}
	
	public String convert(String org) throws NotChordException, NotSuffixException{
		String base = "";
		String suf = "";
		String[] chordList;
		try{
			if(org.charAt(1) == '#' || org.charAt(1) == 'b'){
				base = org.substring(0, 2);
				suf = org.substring(2);
			}else{
				base = org.substring(0, 1);
				suf = org.substring(1);
			}
		}catch(Exception e){
			e.toString();
		}
		
		try{
			this.chord.get(org);
		}catch(Exception e){
			throw new NotChordException(org);
		}
		
		
		if( ! Arrays.asList(this.suffix).contains(suf)){
			throw new NotSuffixException(suf);
		}
		
		if(base.indexOf('b') > 0){
			chordList = this.chordF;
		}else{
			chordList = this.chordS;
		}
		int hanon = this.chord.get(base) + this.diff;
		
		if(hanon > 12){
			hanon = hanon%12;
		}
		return chordList[hanon] + suf;
	}
}

class NotChordException extends Exception{
	private String errorChord;
	public NotChordException(String str){
		this.errorChord = str;
	}
	public String toString(){
		return "'" + this.errorChord + "' is not chord.";
	}
}

class NotSuffixException extends Exception{
	private String errorSuffix;
	public NotSuffixException(String str){
		this.errorSuffix = str;
	}
	public String toString(){
		return "'" + this.errorSuffix + "' is not suffix.";
	}
}