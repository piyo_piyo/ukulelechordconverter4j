Description
===========

Java用のウクレレのコード変換ライブラリです。

Usage
=====

```
import ukulelechordconverter;

String beforeKey = "C";
String afterKey = "F";
UkuleleChordConverter converter = new UkuleleChordConverter(beforeKey, afterKey);

System.out.println(converter.convert("Am7");
System.out.println(converter.convert("Bb");
```
